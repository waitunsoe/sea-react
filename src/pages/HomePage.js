/* eslint-disable jsx-a11y/anchor-is-valid */
import Footer from "../components/footer/Footer";

const HomePage = () => {
  return (
    <>
      <section className="latest-news container">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="title pt-4 pb-3">
                <h1 className="text-uppercase">Latest News</h1>
              </div>
            </div>
            <div className="col-12 col-lg-8">
              <div className="main-card">
                <div className=" card text-bg-dark mb-5">
                  <a href="#" className="">
                    <img
                      src="../assets/images/latest_news_1.jpg"
                      className="card-img"
                      alt="..."
                    />
                  </a>
                  <div className="card-img-overlay p-md-5">
                    <h5 className="card-title mb-md-3">
                      <a href="#" className="text-decoration-none text-white">
                        NFU calls for fertiliser prices to be published
                        'immediately' to boost transparency
                      </a>
                    </h5>
                    <p className="card-text">
                      <i className="bi bi-calendar"></i>
                      <span className="ms-2">Feb 22</span>
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-12 col-lg-4">
              <div className="side-cards row g-xl-2 g-xxl-3">
                <div className="col-12">
                  <div className="side-card-one">
                    <div className="card text-bg-dark mb-5">
                      <a href="#" className="">
                        <img
                          src="../assets/images/latest_news_2.jpg"
                          className="card-img"
                          alt="..."
                        />
                      </a>
                      <div className="card-img-overlay p-md-5 p-lg-3">
                        <h5 className="card-title mb-md-3">
                          <a
                            href="#"
                            className="text-decoration-none text-white"
                          >
                            NFU calls for fertiliser prices to be published
                            'immediately' to boost transparency
                          </a>
                        </h5>
                        <p className="card-text">
                          <i className="bi bi-calendar"></i>
                          <span className="ms-2">Feb 22</span>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-12">
                  <div className="side-card-two">
                    <div className="card text-bg-dark mb-5">
                      <a href="#" className="">
                        <img
                          src="../assets/images/latest_news_.jpg"
                          className="card-img"
                          alt="..."
                        />
                      </a>
                      <div className="card-img-overlay p-md-5 p-lg-3">
                        <h5 className="card-title mb-md-3">
                          <a
                            href="#"
                            className="text-decoration-none text-white"
                          >
                            NFU calls for fertiliser prices to be published
                            'immediately' to boost transparency
                          </a>
                        </h5>
                        <p className="card-text">
                          <i className="bi bi-calendar"></i>
                          <span className="ms-2">Feb 22</span>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="agriculture-news container">
        <div className="row">
          <div className="col-12 col-lg-10">
            <div className="title pt-4 pb-3">
              <h1 className="text-uppercase">Agriculture News</h1>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12 col-lg-10">
            <div className="row">
              <div className="col-12 col-lg-6">
                <div className="big-post-card">
                  <div className="card border-0 shadow-sm">
                    <img
                      src="../image/agri-news-1.png"
                      alt="agri-news-1.png"
                      className="card-img-top"
                    />
                    <div className="card-body">
                      <h5 className="card-title mb-md-3">
                        <a href="#" className="text-decoration-none">
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit.
                        </a>
                      </h5>
                      <p className="card-text">
                        <i className="bi bi-calendar"></i>
                        <span className="ms-2">Feb 22</span>
                      </p>
                      <p className="card-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Nullam proin amet eget semper erat erat massa. Amet
                        habitant dolor feugiat sit amet.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-12 col-lg-6">
                <div className="small-post-card">
                  <div className="card border-0 shadow my-5 mt-lg-0">
                    <div className="row g-0 align-items-start">
                      <div className="col-12 col-sm-4 col-md-3 col-lg-4">
                        <a href="#" className="">
                          <img
                            src="../image/small-card-img-1.png"
                            className="img-fluid w-100"
                            alt="..."
                          />
                        </a>
                      </div>
                      <div className="col-12 col-sm-8 col-md-6 col-lg-8">
                        <div className="card-body py-sm-0">
                          <h5 className="card-title">
                            <a href="#" className="">
                              Lorem ipsum dolor sit amet, consectetur adipiscing
                              elit.
                            </a>
                          </h5>
                          <p className="card-text bg-light w-50">
                            <small className="">
                              <i className="bi bi-calendar"></i>
                              <span className="ms-2">Feb 22</span>
                            </small>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card border-0 shadow my-5">
                    <div className="row g-0 align-items-start">
                      <div className="col-12 col-sm-4 col-md-3 col-lg-4">
                        <a href="#" className="">
                          <img
                            src="../image/small-card-img-2.png"
                            className="img-fluid w-100"
                            alt="..."
                          />
                        </a>
                      </div>
                      <div className="col-12 col-sm-8 col-md-6 col-lg-8">
                        <div className="card-body py-sm-0">
                          <h5 className="card-title">
                            <a href="#" className="">
                              Lorem ipsum dolor sit amet, consectetur adipiscing
                              elit.
                            </a>
                          </h5>
                          <p className="card-text bg-light w-50">
                            <small className="">
                              <i className="bi bi-calendar"></i>
                              <span className="ms-2">Feb 22</span>
                            </small>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card border-0 shadow my-5">
                    <div className="row g-0 align-items-start">
                      <div className="col-12 col-sm-4 col-md-3 col-lg-4">
                        <a href="#" className="">
                          <img
                            src="../image/small-card-img-3.png"
                            className="img-fluid w-100"
                            alt="..."
                          />
                        </a>
                      </div>
                      <div className="col-12 col-sm-8 col-md-6 col-lg-8">
                        <div className="card-body py-sm-0">
                          <h5 className="card-title">
                            <a href="#" className="">
                              Lorem ipsum dolor sit amet, consectetur adipiscing
                              elit.
                            </a>
                          </h5>
                          <p className="card-text bg-light w-50">
                            <small className="">
                              <i className="bi bi-calendar"></i>
                              <span className="ms-2">Feb 22</span>
                            </small>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card border-0 shadow my-5">
                    <div className="row g-0 align-items-start">
                      <div className="col-12 col-sm-4 col-md-3 col-lg-4">
                        <a href="#" className="">
                          <img
                            src="../image/small-card-img-4.png"
                            className="img-fluid w-100"
                            alt="..."
                          />
                        </a>
                      </div>
                      <div className="col-12 col-sm-8 col-md-6 col-lg-8">
                        <div className="card-body py-sm-0">
                          <h5 className="card-title">
                            <a href="#" className="">
                              Lorem ipsum dolor sit amet, consectetur adipiscing
                              elit.
                            </a>
                          </h5>
                          <p className="card-text bg-light w-50">
                            <small className="">
                              <i className="bi bi-calendar"></i>
                              <span className="ms-2">Feb 22</span>
                            </small>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-12">
                <div className="card border-0 rounded-5 py-5">
                  <img
                    src="../image/leader-board.png"
                    alt="leader-board.png"
                    className="img-fluid w-100 rounded"
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-lg-2">
            <div className="row">
              <div className="col-4 col-lg-12">
                <div className="card border-0 rounded mb-lg-5">
                  <img
                    src="../image/ads.png"
                    alt="ads.png"
                    className="img-fluid w-100"
                  />
                </div>
              </div>
              <div className="col-4 col-lg-12">
                <div className="card border-0 rounded my-lg-5">
                  <img
                    src="../image/ads.png"
                    alt="ads.png"
                    className="img-fluid w-100"
                  />
                </div>
              </div>
              <div className="col-4 col-lg-12">
                <div className="card border-0 rounded my-lg-5">
                  <img
                    src="../image/ads.png"
                    alt="ads.png"
                    className="img-fluid w-100"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="agriculture-news container">
        <div className="row">
          <div className="col-12 col-lg-10">
            <div className="title pt-4 pb-3">
              <h1 className="text-uppercase">Fishery News</h1>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12 col-lg-10">
            <div className="row">
              <div className="col-12 col-lg-6">
                <div className="big-post-card">
                  <div className="card border-0 shadow-sm">
                    <img
                      src="../image/agri-news-1.png"
                      alt="agri-news-1.png"
                      className="card-img-top"
                    />
                    <div className="card-body">
                      <h5 className="card-title mb-md-3">
                        <a href="#" className="text-decoration-none">
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit.
                        </a>
                      </h5>
                      <p className="card-text">
                        <i className="bi bi-calendar"></i>
                        <span className="ms-2">Feb 22</span>
                      </p>
                      <p className="card-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Nullam proin amet eget semper erat erat massa. Amet
                        habitant dolor feugiat sit amet.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-12 col-lg-6">
                <div className="small-post-card">
                  <div className="card border-0 shadow my-5 mt-lg-0">
                    <div className="row g-0 align-items-start">
                      <div className="col-12 col-sm-4 col-md-3 col-lg-4">
                        <a href="#" className="">
                          <img
                            src="../image/small-card-img-1.png"
                            className="img-fluid w-100"
                            alt="..."
                          />
                        </a>
                      </div>
                      <div className="col-12 col-sm-8 col-md-6 col-lg-8">
                        <div className="card-body py-sm-0">
                          <h5 className="card-title">
                            <a href="#" className="">
                              Lorem ipsum dolor sit amet, consectetur adipiscing
                              elit.
                            </a>
                          </h5>
                          <p className="card-text bg-light w-50">
                            <small className="">
                              <i className="bi bi-calendar"></i>
                              <span className="ms-2">Feb 22</span>
                            </small>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card border-0 shadow my-5">
                    <div className="row g-0 align-items-start">
                      <div className="col-12 col-sm-4 col-md-3 col-lg-4">
                        <a href="#" className="">
                          <img
                            src="../image/small-card-img-2.png"
                            className="img-fluid w-100"
                            alt="..."
                          />
                        </a>
                      </div>
                      <div className="col-12 col-sm-8 col-md-6 col-lg-8">
                        <div className="card-body py-sm-0">
                          <h5 className="card-title">
                            <a href="#" className="">
                              Lorem ipsum dolor sit amet, consectetur adipiscing
                              elit.
                            </a>
                          </h5>
                          <p className="card-text bg-light w-50">
                            <small className="">
                              <i className="bi bi-calendar"></i>
                              <span className="ms-2">Feb 22</span>
                            </small>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card border-0 shadow my-5">
                    <div className="row g-0 align-items-start">
                      <div className="col-12 col-sm-4 col-md-3 col-lg-4">
                        <a href="#" className="">
                          <img
                            src="../image/small-card-img-3.png"
                            className="img-fluid w-100"
                            alt="..."
                          />
                        </a>
                      </div>
                      <div className="col-12 col-sm-8 col-md-6 col-lg-8">
                        <div className="card-body py-sm-0">
                          <h5 className="card-title">
                            <a href="#" className="">
                              Lorem ipsum dolor sit amet, consectetur adipiscing
                              elit.
                            </a>
                          </h5>
                          <p className="card-text bg-light w-50">
                            <small className="">
                              <i className="bi bi-calendar"></i>
                              <span className="ms-2">Feb 22</span>
                            </small>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card border-0 shadow my-5">
                    <div className="row g-0 align-items-start">
                      <div className="col-12 col-sm-4 col-md-3 col-lg-4">
                        <a href="#" className="">
                          <img
                            src="../image/small-card-img-4.png"
                            className="img-fluid w-100"
                            alt="..."
                          />
                        </a>
                      </div>
                      <div className="col-12 col-sm-8 col-md-6 col-lg-8">
                        <div className="card-body py-sm-0">
                          <h5 className="card-title">
                            <a href="#" className="">
                              Lorem ipsum dolor sit amet, consectetur adipiscing
                              elit.
                            </a>
                          </h5>
                          <p className="card-text bg-light w-50">
                            <small className="">
                              <i className="bi bi-calendar"></i>
                              <span className="ms-2">Feb 22</span>
                            </small>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-12">
                <div className="card border-0 rounded-5 py-5">
                  <img
                    src="../image/leader-board.png"
                    alt="leader-board.png"
                    className="img-fluid w-100 rounded"
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-lg-2">
            <div className="row">
              <div className="col-4 col-lg-12">
                <div className="card border-0 rounded mb-lg-5">
                  <img
                    src="../image/ads.png"
                    alt="ads.png"
                    className="img-fluid w-100"
                  />
                </div>
              </div>
              <div className="col-4 col-lg-12">
                <div className="card border-0 rounded my-lg-5">
                  <img
                    src="../image/ads.png"
                    alt="ads.png"
                    className="img-fluid w-100"
                  />
                </div>
              </div>
              <div className="col-4 col-lg-12">
                <div className="card border-0 rounded my-lg-5">
                  <img
                    src="../image/ads.png"
                    alt="ads.png"
                    className="img-fluid w-100"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="agriculture-news container">
        <div className="row">
          <div className="col-12 col-lg-10">
            <div className="title pt-4 pb-3">
              <h1 className="text-uppercase">Livestock News</h1>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12 col-lg-10">
            <div className="row">
              <div className="col-12 col-lg-6">
                <div className="big-post-card">
                  <div className="card border-0 shadow-sm">
                    <img
                      src="../image/agri-news-1.png"
                      alt="agri-news-1.png"
                      className="card-img-top"
                    />
                    <div className="card-body">
                      <h5 className="card-title mb-md-3">
                        <a href="#" className="text-decoration-none">
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit.
                        </a>
                      </h5>
                      <p className="card-text">
                        <i className="bi bi-calendar"></i>
                        <span className="ms-2">Feb 22</span>
                      </p>
                      <p className="card-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Nullam proin amet eget semper erat erat massa. Amet
                        habitant dolor feugiat sit amet.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-12 col-lg-6">
                <div className="small-post-card">
                  <div className="card border-0 shadow my-5 mt-lg-0">
                    <div className="row g-0 align-items-start">
                      <div className="col-12 col-sm-4 col-md-3 col-lg-4">
                        <a href="#" className="">
                          <img
                            src="../image/small-card-img-1.png"
                            className="img-fluid w-100"
                            alt="..."
                          />
                        </a>
                      </div>
                      <div className="col-12 col-sm-8 col-md-6 col-lg-8">
                        <div className="card-body py-sm-0">
                          <h5 className="card-title">
                            <a href="#" className="">
                              Lorem ipsum dolor sit amet, consectetur adipiscing
                              elit.
                            </a>
                          </h5>
                          <p className="card-text bg-light w-50">
                            <small className="">
                              <i className="bi bi-calendar"></i>
                              <span className="ms-2">Feb 22</span>
                            </small>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card border-0 shadow my-5">
                    <div className="row g-0 align-items-start">
                      <div className="col-12 col-sm-4 col-md-3 col-lg-4">
                        <a href="#" className="">
                          <img
                            src="../image/small-card-img-2.png"
                            className="img-fluid w-100"
                            alt="..."
                          />
                        </a>
                      </div>
                      <div className="col-12 col-sm-8 col-md-6 col-lg-8">
                        <div className="card-body py-sm-0">
                          <h5 className="card-title">
                            <a href="#" className="">
                              Lorem ipsum dolor sit amet, consectetur adipiscing
                              elit.
                            </a>
                          </h5>
                          <p className="card-text bg-light w-50">
                            <small className="">
                              <i className="bi bi-calendar"></i>
                              <span className="ms-2">Feb 22</span>
                            </small>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card border-0 shadow my-5">
                    <div className="row g-0 align-items-start">
                      <div className="col-12 col-sm-4 col-md-3 col-lg-4">
                        <a href="#" className="">
                          <img
                            src="../image/small-card-img-3.png"
                            className="img-fluid w-100"
                            alt="..."
                          />
                        </a>
                      </div>
                      <div className="col-12 col-sm-8 col-md-6 col-lg-8">
                        <div className="card-body py-sm-0">
                          <h5 className="card-title">
                            <a href="#" className="">
                              Lorem ipsum dolor sit amet, consectetur adipiscing
                              elit.
                            </a>
                          </h5>
                          <p className="card-text bg-light w-50">
                            <small className="">
                              <i className="bi bi-calendar"></i>
                              <span className="ms-2">Feb 22</span>
                            </small>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card border-0 shadow my-5">
                    <div className="row g-0 align-items-start">
                      <div className="col-12 col-sm-4 col-md-3 col-lg-4">
                        <a href="#" className="">
                          <img
                            src="../image/small-card-img-4.png"
                            className="img-fluid w-100"
                            alt="..."
                          />
                        </a>
                      </div>
                      <div className="col-12 col-sm-8 col-md-6 col-lg-8">
                        <div className="card-body py-sm-0">
                          <h5 className="card-title">
                            <a href="#" className="">
                              Lorem ipsum dolor sit amet, consectetur adipiscing
                              elit.
                            </a>
                          </h5>
                          <p className="card-text bg-light w-50">
                            <small className="">
                              <i className="bi bi-calendar"></i>
                              <span className="ms-2">Feb 22</span>
                            </small>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-12">
                <div className="card border-0 rounded-5 py-5">
                  <img
                    src="../image/leader-board.png"
                    alt="leader-board.png"
                    className="img-fluid w-100 rounded"
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-lg-2">
            <div className="row">
              <div className="col-4 col-lg-12">
                <div className="card border-0 rounded mb-lg-5">
                  <img
                    src="../image/ads.png"
                    alt="ads.png"
                    className="img-fluid w-100"
                  />
                </div>
              </div>
              <div className="col-4 col-lg-12">
                <div className="card border-0 rounded my-lg-5">
                  <img
                    src="../image/ads.png"
                    alt="ads.png"
                    className="img-fluid w-100"
                  />
                </div>
              </div>
              <div className="col-4 col-lg-12">
                <div className="card border-0 rounded my-lg-5">
                  <img
                    src="../image/ads.png"
                    alt="ads.png"
                    className="img-fluid w-100"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="container mb-5" id="upcoming-event">
        <div className="row">
          <div className="col-12 col-lg-10">
            <div className="title pt-4 pb-3">
              <h1 className="text-uppercase">Upcoming Events</h1>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <div className="upcoming-event-img-box">
              <img
                src="../image/upcoming-event.png"
                alt="upcoming-event.png"
                className="img-fluid w-100"
              />
            </div>
          </div>
        </div>
      </section>

      <section className="my-5" id="magazine">
        <div className="container">
          <div className="row">
            <div className="col-12 col-lg-10">
              <div className="title py-5">
                <h1 className="text-uppercase">Magazines</h1>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12 col-md-6 col-lg-3">
              <div className="magazine-book h-100 pb-5">
                <a href="#" className="text-decoration-none text-muted">
                  <img
                    src="../image/magazine-1.png"
                    alt="magazine-1"
                    className="card-img-top w-100 h-75"
                  />
                </a>
                <div className="text-center text-uppercase pb-5">
                  <a href="#" className="btn btn-warning">
                    Download <i className="bi bi-download ms-2"></i>
                  </a>
                </div>
              </div>
            </div>
            <div className="col-12 col-md-6 col-lg-3">
              <div className="magazine-book h-100 pb-5">
                <a href="#" className="text-decoration-none text-muted">
                  <img
                    src="../image/magazine-2.png"
                    alt="magazine-1"
                    className="card-img-top w-100 h-75"
                  />
                </a>
                <div className="text-center text-uppercase pb-5">
                  <a href="#" className="btn btn-warning">
                    Download <i className="bi bi-download ms-2"></i>
                  </a>
                </div>
              </div>
            </div>
            <div className="col-12 col-md-6 col-lg-3">
              <div className="magazine-book h-100 pb-5">
                <a href="#" className="text-decoration-none text-muted">
                  <img
                    src="../image/magazine-3.png"
                    alt="magazine-1"
                    className="card-img-top w-100 h-75"
                  />
                </a>
                <div className="text-center text-uppercase pb-5">
                  <a href="#" className="btn btn-warning">
                    Download <i className="bi bi-download ms-2"></i>
                  </a>
                </div>
              </div>
            </div>
            <div className="col-12 col-md-6 col-lg-3">
              <div className="magazine-book h-100 pb-5">
                <a href="#" className="text-decoration-none text-muted">
                  <img
                    src="../image/magazine-4.png"
                    alt="magazine-1"
                    className="card-img-top w-100 h-75"
                  />
                </a>
                <div className="text-center text-uppercase pb-5">
                  <a href="#" className="btn btn-warning">
                    Download <i className="bi bi-download ms-2"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="subscription my-5 pb-5">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-12 col-md-10 col-lg-7">
              <div className="text-center">
                <h1 className="title display-5 text-capitalize">Subscribe</h1>
                <h4 className="sub-title display-6 fw-lighter">
                  to our magazine
                </h4>
                <p className="text text-black-50">
                  Sapien laoreet nibh diam imperdiet massa.
                </p>
              </div>
              <div className="input-group">
                <input
                  type="email"
                  name="email"
                  id="email"
                  className="form-control"
                  placeholder="Email address"
                />
                <button
                  type="submit"
                  className="btn btn-warning text-uppercase"
                >
                  subscribe
                </button>
              </div>
            </div>
          </div>
        </div>
      </section>

      <Footer />
    </>
  );
};

export default HomePage;
