import { Link } from "react-router-dom";

const Navigation = () => {
  const links = [
    { name: "Home", path: "/" },
    { name: "Agri News", path: "agri-news" },
    { name: "Livestock News", path: "livestock-news" },
    { name: "Fishery News", path: "fishery-news" },
    { name: "Shows", path: "shows" },
    { name: "Shows", path: "shows" },
    { name: "Directory/Buyer's Guide", path: "buyer-guide" },
    { name: "Magazines", path: "magazines" },
    { name: "Advertise", path: "advertise" },
    { name: "Contact", path: "contact" },
    { name: "About", path: "about" },
  ];

  return (
    <>
      <header id="top-header">
        <div class="container">
          <div class="row align-items-center p-2">
            <div class="col-12 col-xl-6">
              <h1 class="top-header-title text-uppercase mb-0 text-center text-xl-start">
                South East Asia <span class="fw-bold">Agriculture</span>
              </h1>
            </div>
            <div class="col-12 col-xl-6">
              <div class="top-header-others d-flex justify-content-between  justify-content-xl-end align-items-center pe-lg-3 pt-lg-3">
                <p class="phone-number mb-0">
                  <i class="bi bi-telephone-fill"></i>
                  <span class="ms-1">
                    {" "}
                    Hotline{" "}
                    <span class="d-block d-md-inline">
                      +959 7308989005
                    </span>{" "}
                  </span>
                </p>
                <div class="language-select mb-0 mx-4 ms-sm-5">
                  <div class="select-box">
                    <select
                      class="form-select"
                      aria-label="Default select example"
                    >
                      <option selected>English</option>
                      <option value="1">Myanmar</option>
                      <option value="2">Japan</option>
                    </select>
                  </div>
                </div>
                <p class="login-box mb-0 ms-3 ms-sm-5 ">
                  <i class="bi bi-person fs-5"></i>
                </p>
              </div>
            </div>
          </div>
        </div>
      </header>
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
          <button
            class="navbar-toggler ms-auto border-0"
            type="button"
            data-bs-toggle="offcanvas"
            data-bs-target="#offcanvasNavbar"
            aria-controls="offcanvasNavbar"
          >
            <i class="bi bi-list"></i>
          </button>
          <div
            class="offcanvas offcanvas-end text-bg-dark"
            tabindex="-1"
            id="offcanvasNavbar"
            aria-labelledby="offcanvasNavbarLabel"
          >
            <div class="offcanvas-header">
              <h5 class="offcanvas-title d-none" id="offcanvasNavbarLabel">
                South East Asia Agriculture
              </h5>
              <button
                type="button"
                class="btn-close btn-close-white"
                data-bs-dismiss="offcanvas"
                aria-label="Close"
              ></button>
            </div>
            <div class="offcanvas-body">
              <ul class="navbar-nav justify-content-between flex-grow-1 pe-3">
                {links.map((link, index) => {})}

                <li class="nav-item">
                  <a
                    class="nav-link text-uppercase active"
                    aria-current="page"
                    href="#"
                  >
                    Home
                  </a>
                </li>
                <li class="nav-item dropdown">
                  <a
                    class="nav-link text-uppercase dropdown-toggle"
                    href="#"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    News
                  </a>
                  <ul class="dropdown-menu">
                    <li>
                      <a class="dropdown-item" href="#">
                        Agri News
                      </a>
                    </li>
                    <li>
                      <a class="dropdown-item" href="#">
                        Livestock News
                      </a>
                    </li>
                    <li>
                      <a class="dropdown-item" href="#">
                        Fishery News
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="nav-item">
                  <a
                    class="nav-link text-uppercase"
                    aria-current="page"
                    href="#"
                  >
                    Shows
                  </a>
                </li>
                <li class="nav-item">
                  <a
                    class="nav-link text-uppercase"
                    aria-current="page"
                    href="#"
                  >
                    Directory/Buyer's Guide
                  </a>
                </li>
                <li class="nav-item">
                  <a
                    class="nav-link text-uppercase"
                    aria-current="page"
                    href="#"
                  >
                    Magazines
                  </a>
                </li>
                <li class="nav-item">
                  <a
                    class="nav-link text-uppercase"
                    aria-current="page"
                    href="#"
                  >
                    Advertie
                  </a>
                </li>
                <li class="nav-item">
                  <a
                    class="nav-link text-uppercase"
                    aria-current="page"
                    href="#"
                  >
                    Contact
                  </a>
                </li>
                <li class="nav-item">
                  <a
                    class="nav-link text-uppercase"
                    aria-current="page"
                    href="#"
                  >
                    Abouts
                  </a>
                </li>
                <li class="nav-item">
                  <a
                    class="nav-link text-uppercase"
                    aria-current="page"
                    href="#"
                  >
                    <i class="bi bi-search"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
    </>
  );
};

export default Navigation;
